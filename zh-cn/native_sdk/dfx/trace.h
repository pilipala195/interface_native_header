/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HIVIEWDFX_HITRACE_H
#define HIVIEWDFX_HITRACE_H
/**
 * @addtogroup Hitrace
 * @{
 *
 * @brief hiTraceMeter为开发者提供系统性能打点接口。
 *
 * 开发者通过在自己的业务逻辑中的关键代码位置调用HiTraceMeter系统跟踪提供的API接口，能够有效进行关键执行流程耗时度量和问题定位。
 *
 * @syscap SystemCapability.HiviewDFX.HiTrace
 *
 * @since 10
 */

/**
 * @file trace.h
 *
 * @brief HiTraceMeterH模块打点接口定义，通过这些接口实现性能打点相关功能。
 *
 * 使用示例:\n
 * 同步时间片跟踪事件：\n
 *     OH_HiTrace_StartTrace("hitraceTest");\n
 * 	   OH_HiTrace_FinishTrace();\n
 * 结果输出：\n
 *     <...>-1668    (-------) [003] ....   135.059377: tracing_mark_write: B|1668|H:hitraceTest\n
 *     <...>-1668    (-------) [003] ....   135.059415: tracing_mark_write: E|1668|\n
 * 异步时间片跟踪事件：\n
 * 	   OH_HiTrace_StartAsyncTrace("hitraceTest", 123);\n
 *     OH_HiTrace_FinishAsyncTrace("hitraceTest", 123);\n
 * 结果输出：\n
 *     <...>-2477    (-------) [001] ....   396.427165: tracing_mark_write: S|2477|H:hitraceTest 123\n
 *     <...>-2477    (-------) [001] ....   396.427196: tracing_mark_write: F|2477|H:hitraceTest 123\n
 * 整数值跟踪事件：\n
 *     OH_HiTrace_CountTrace("hitraceTest", 500);\n
 * 结果输出：\n
 *     <...>-2638    (-------) [002] ....   458.904382: tracing_mark_write: C|2638|H:hitraceTest 500\n
 *
 * @since 10
 */
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 标记一个同步跟踪耗时任务的开始。
 *
 * 同步跟踪打点接口OH_HiTrace_StartTrace和OH_HiTrace_FinishTrace必须配对使用。
 * OH_HiTrace_StartTrace和OH_HiTrace_FinishTrace函数对可以以嵌套模式使用，跟踪数据解析时使用栈式数据结构进行匹配。
 *
 * @param name 跟踪的名字。
 *
 * @since 10
 */
void OH_HiTrace_StartTrace(const char *name);

/**
 * @brief 标记一个同步跟踪耗时任务的结束。
 *
 * 必须和OH_HiTrace_StartTrace配对使用。跟踪数据解析时，和其前执行流程中最近的OH_HiTrace_StartTrace进行匹配。
 *
 * @since 10
 */
void OH_HiTrace_FinishTrace(void);

/**
 * @brief 标记一个异步跟踪耗时任务的开始。
 *
 * 用于在异步操作前调用进行开始打点，异步跟踪开始和结束数据由于不是顺序发生的，所以解析时需要通过一个唯一的taskId进行识别，这个taskId作为异步接口的参数传入。
 * 和OH_HiTrace_FinishAsyncTrace配对使用，参数name和taskId相同的这两个接口调用匹配成一个异步跟踪耗时任务。
 * 如果有多个相同name的任务需要跟踪或者对同一个任务跟踪多次，并且任务同时被执行，则每次调用startTrace的taskId不相同。
 * 如果具有相同name的任务是串行执行的，则taskId可以相同。
 *
 * @param name 异步跟踪的名字。
 * @param taskId 异步跟踪的ID。 异步跟踪开始和结束由于不是顺序发生的，所以需要通过name和每次执行唯一的taskId进行开始和结束的匹配。
 *
 * @since 10
 */
void OH_HiTrace_StartAsyncTrace(const char *name, int32_t taskId);

/**
 * @brief 标记一个异步跟踪耗时任务的结束。
 *
 * 在异步操作完成后如回调函数中调用，进行结束打点。
 * 和OH_HiTrace_StartAsyncTrace配对使用，参数name和taskId必须与异步跟踪的开始打点接口OH_HiTrace_StartAsyncTrace的对应参数值一致。
 *
 * @param name 异步跟踪的名字。
 * @param taskId 异步跟踪的ID。异步跟踪开始和结束由于不是顺序发生的，所以需要通过name和每次执行唯一的taskId进行开始和结束的匹配。
 *
 * @since 10
 */
void OH_HiTrace_FinishAsyncTrace(const char *name, int32_t taskId);

/**
 * @brief 用于跟踪给定整数变量名和整数值。
 *
 * 多次执行该接口可以跟踪给定整数变量在不同时刻的数值变化。
 *
 * @param name 整数变量跟踪的名字，不必与真实变量名相同。
 * @param count 整数数值，一般可以传入整数变量。
 *
 * @since 10
 */
void OH_HiTrace_CountTrace(const char *name, int64_t count);

#ifdef __cplusplus
}
#endif
#endif // HIVIEWDFX_HITRACE_H