/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef VIBRATOR_TYPE_H
#define VIBRATOR_TYPE_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
* @brief 为用户定义错误码。
*
* @since 11
*/
typedef enum Vibrator_ErrorCode : int32_t {
    /**< 权限校验失败。 */
    PERMISSION_DENIED = 201,
    /**< 参数检查失败，包括必选参数没有传入，参数类型错误等。 */
    PARAMETER_ERROR = 401,
    /**< 该设备不支持此 API，通常用于在设备已支持该 SysCap 时，针对其少量的 API 的支持处理。 */
    UNSUPPORTED = 801,
    /**< 设备操作失败。 */
    DEVICE_OPERATION_FAILED = 14600101,
} Vibrator_ErrorCode;

/**
 * @brief 振动优先级。
 *
 * @since 11
 */
typedef enum Vibrator_Usage {
    /**< 未知场景 */
    USAGE_UNKNOWN = 0,
    /**< 报警 */
    USAGE_ALARM = 1,
    /**< 铃声 */
    USAGE_RING = 2,
     /**< 通知 */
    USAGE_NOTIFICATION = 3,
    /**< 通信 */
    USAGE_COMMUNICATION = 4,
    /**< 触摸 */
    USAGE_TOUCH = 5,
    /**< 媒体 */
    USAGE_MEDIA = 6,
    /**< 物理反馈 */
    USAGE_PHYSICAL_FEEDBACK = 7,
    /**< 模拟现实 */
    USAGE_SIMULATE_REALITY = 8,
    USAGE_MAX = 9
} Vibrator_Usage;

/**
 * @brief 马达属性。
 *
 * @since 11
 */
typedef struct Vibrator_Attribute {
    /**< 马达ID */
    int32_t id;
    /**< 振动场景 */
    Vibrator_Usage usage;
} Vibrator_Attribute;

/**
 * @brief 振动文件描述。
 *
 * @since 11
 */
typedef struct Vibrator_FileDescription {
    /**< 自定义振动序列的文件句柄。 */
    int32_t fd;
    /**< 自定义振动序列的偏移地址。 */
    int64_t offset;
    /**< 自定义振动序列的总长度。 */
    int64_t length;
} Vibrator_FileDescription;
/** @} */
#ifdef __cplusplus
};
#endif

#endif  // endif VIBRATOR_TYPE_H