/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup battery
 * @{
 *
 * @brief 提供获取、订阅电池信息的接口。
 *
 * 电池模块为电池服务提供的获取、订阅电池信息的接口。
 * 服务获取此模块的对象或代理后，可以调用相关的接口获取、订阅电池信息。
 *
 * @since 3.1
 * @version 1.0
 */

/**
 * @file IBatteryCallback.idl
 *
 * @brief 电池信息的回调。
 *
 * 电池模块为电池服务提供的订阅电池信息变化的回调。
 *
 * @since 3.1
 * @version 1.0
 */

package ohos.hdi.battery.v1_0;

import ohos.hdi.battery.v1_0.Types;

/**
 * @brief 电池信息的回调。
 *
 * 服务创建此回调对象后，可以调用{@link IBatteryInterface}的接口注册回调，从而订阅电池信息的变化。
 *
 * @since 3.1
 */
[callback] interface IBatteryCallback {

    /**
     * @brief 电池信息的回调方法。
     *
     * 当电池信息发生变化时，将通过此方法的参数返回给服务。
     *
     * @param event 电池信息，如电量，电压，健康状态等。
     * @see BatteryInfo
     *
     * @since 3.1
     */
    Update([in] struct BatteryInfo event);
}
/** @} */
