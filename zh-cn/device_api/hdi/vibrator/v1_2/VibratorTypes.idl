/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Vibrator
 * @{
 *
 * @brief 马达驱动对马达服务提供通用的接口能力。
 *
 * 模块提供马达服务对马达驱动访问的统一接口，服务获取驱动对象或者代理后，控制马达的单次振动、周期性振动、高清振动、停止振动、设置马达振幅与频率。
 *
 * @since 4.1
 * @version 1.2
 */

/**
 * @file VibratorTypes.idl
 *
 * @brief 定义马达数据结构，包括马达振动模式和马达参数。
 *
 * @since 4.1
 * @version 1.2
 */

/**
 *
 * @brief 马达模块的包路径。
 *
 * @since 4.1
 * @version 1.2
 */

package ohos.hdi.vibrator.v1_2;

import ohos.hdi.vibrator.v1_1.VibratorTypes;

/**
 * @brief 表示振动类型。
 *
 * @since 4.1
 */
enum EVENT_TYPE {
    /**< 表示振动是连续的。 */
    CONTINUOUS = 0,
    /**< 表示振动是瞬时的。 */
    TRANSIENT  = 1,
};

/**
 * @brief 表示StopV1_2接口可传入参数枚举。
 *
 * @since 4.1
 */
enum HdfVibratorModeV1_2 {
    /**< 表示给定持续时间内的单次振动。 */
    HDF_VIBRATOR_MODE_ONCE,
    /**< 表示具有预置效果的周期性振动。 */
    HDF_VIBRATOR_MODE_PRESET,
    /**< 表示高清振动。 */
    HDF_VIBRATOR_MODE_HDHAPTIC,
    /**< 表示效果模式无效。 */
    HDF_VIBRATOR_MODE_BUTT,
};

/**
 * @brief 表示振动点。
 *
 * 参数包含时间，强度和频率。
 *
 * @since 4.1
 */
struct CurvePoint {
    /** 时间。 */
    int time;
    /** 强度。 */
    int intensity;
    /** 频率。 */
    int frequency;
};

/**
 * @brief 表示振动事件。
 *
 * 参数包含振动时间，强度，频率等等。
 *
 * @since 4.1
 */
struct HapticEvent {
    /** 振动类型。 */
    enum EVENT_TYPE type;
    /** 时间。 */
    int time;
    /** 振动延时。 */
    int duration;
    /** 振动强度。 */
    int intensity;
    /** 振动频率。 */
    int frequency;
    /** 马达id。表示振动的是哪个马达。 */
    int index;
    /** 振动点数量。 */
    int pointNum;
    /** 振动点数组。 */
    struct CurvePoint[] points;
};

/**
 * @brief 高清振动数据包。
 *
 * 参数包含具体的高清振动数据。
 *
 * @since 4.1
 */
struct HapticPaket {
    /** 时间。 */
    int time;
    /** 振动事件数量。 */
    int eventNum;
    /** 振动事件数组。 */
    struct HapticEvent[] events;
};

/**
 * @brief 振动能力数据包。
 *
 * 信息包括不同类型的振动。
 *
 * @since 4.1
 */
struct HapticCapacity {
    /** 是否支持高清振动。 */
    boolean isSupportHdHaptic;
    /** 是否支持预定义振动。 */
    boolean isSupportPresetMapping;
    /** 是否支持延时振动。 */
    boolean isSupportTimeDelay;
    /** 预留参数. */
    boolean reserved0;
    /** 预留参数. */
    int reserved1;
};