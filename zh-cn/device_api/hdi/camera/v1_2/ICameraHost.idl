/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * Camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 4.1
 * @version 1.2
 */

 /**
 * @file ICameraHost.idl
 *
 * @brief Camera服务的管理类，对上层提供HDI接口。
 *
 * @since 4.1
 * @version 1.2
 */

/**
 * @brief Camera设备接口的包路径。
 *
 * @since 4.1
 * @version 1.2
 */
package ohos.hdi.camera.v1_2;

import ohos.hdi.camera.v1_1.ICameraHost;
import ohos.hdi.camera.v1_2.ICameraDevice;
import ohos.hdi.camera.v1_2.ICameraHostCallback;
import ohos.hdi.camera.v1_0.ICameraDeviceCallback;

/**
 * @brief 定义Camera设备功能操作。
 *
 * 打开并执行Camera设备、通知设备状态更改信息、设置回调接口等相关操作。
 */
interface ICameraHost extends ohos.hdi.camera.v1_1.ICameraHost {
    /**
     * @brief 打开Camera设备。
     *
     * 打开指定的Camera设备，通过此接口可以获取到ICameraDevice对象，通过ICameraDevice对象可以操作具体的Camera设备。
     *
     * @param cameraId 需要打开的Camera设备ID，可通过{@link GetCameraIds}接口获取当前已有Camera设备列表。
     * @param callbackObj Camera设备相关的回调函数，具体参见{@link ICameraDeviceCallback}。
     * @param device 返回当前要打开的Camera设备ID对应的ICameraDevice对象。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 4.1
     * @version 1.2
     */
    OpenCameraV1_2([in] String cameraId, [in] ICameraDeviceCallback callbackObj, [out] ICameraDevice device);

    /**
     * @brief 将设备状态更改通知设备供应商。
     *
     * 通过调用此函数，可以通知设备供应商设备状态更改。
     *
     * @param notifyType 通知的类型。
     * @param deviceState 设备的状态。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 4.1
     * @version 1.2
     */
    NotifyDeviceStateChangeInfo([in] int notifyType, [in] int deviceState);

    /**
     * @brief 设置ICameraHost回调接口，回调函数参考{@link ICameraHostCallback}。
     *
     * @param callbackObj 要设置的回调函数。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 4.1
     * @version 1.2
     */
    SetCallbackV1_2([in] ICameraHostCallback callbackObj);

    /**
     * @brief 打开或关闭闪光灯。
     *
     * @param level 指定是打开还是关闭闪光灯。值 1 表示打开闪光灯，0 表示相反。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 4.1
     * @version 1.2
     */
    SetFlashlightV1_2([in] float level);

    /**
     * @brief 切换镜头时预热相机设备。
     *
     * 当用户触摸摄像头应用镜头开关图标时可以调用此功能以加速启动的相机设备，由cameraId指定。
     *
     * @param cameraId 指示相机设备的ID，可以通过调用 {@link GetCameraIds} 获取。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 4.1
     * @version 1.2
     */
    PreCameraSwitch([in] String cameraId);

    /**
     * @brief 预启动Camera设备。
     *
     * 当用户触摸摄像头应用图标时可以调用此函数以加速启动的相机设备，由cameraId指定。
     *
     * @param config 预启动配置. 详细可查看{@link PrelaunchConfig}.
     * @param operationMode 流操作模式. 详细可查看{@link OperationMode}.
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 4.1
     * @version 1.2
     */
    PrelaunchWithOpMode([in] struct PrelaunchConfig config, [in] int operationMode);
}
/** @} */