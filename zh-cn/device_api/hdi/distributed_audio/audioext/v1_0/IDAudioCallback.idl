/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Distributed Audio
 * @{
 *
 * @brief Distributed Audio
 *
 * Distributed Audio模块包括对分布式音频设备的操作、流的操作和各种回调等。
 * 通过IDAudioCallback和IDAudioManager接口，与Source SA通信交互，实现分布式功能。
 *
 * @since 4.1
 * @version 1.0
 */

/**
 * @file IDAudioCallback.idl
 *
 * @brief 分布式音频HDF服务与分布式音频SA服务的传输接口调用，并为上层提供硬件驱动接口。
 *
 * @since 4.1
 * @version 1.0
 */

/**
 * @brief Distributed Audio设备接口的包路径。
 *
 * @since 4.1
 * @version 1.0
 */
package ohos.hdi.distributed_audio.audioext.v1_0;

import ohos.hdi.distributed_audio.audioext.v1_0.Types;

/**
 * @brief 定义Distributed Audio设备基本的操作。
 *
 * 启用和关闭分布式音频设备、设置音频参数、事件通知等相关操作。
 *
 * @since 4.1
 * @version 1.0
 */
[callback] interface IDAudioCallback {
    /**
     * @brief 打开分布式音频设备。
     *
     * @param adpName 分布式音频设备NetworkID。
     * @param devId 分布式音频设备的端口ID。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    OpenDevice([in] String adpName, [in] int devId);
    /**
     * @brief 关闭分布式音频设备。
     *
     * @param adpName 分布式音频设备NetworkID。
     * @param devId 分布式音频设备的端口ID。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    CloseDevice([in] String adpName, [in] int devId);
    /**
     * @brief 设置分布式音频设备参数。
     *
     * @param adpName 分布式音频设备NetworkID。
     * @param devId 分布式音频设备的端口ID。
     * @param param 音频参数(包括采样率、通道数等) 
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    SetParameters([in] String adpName, [in] int devId, [in] struct AudioParameter param);
    /**
     * @brief 向分布式音频SA通知事件。
     *
     * @param adpName 分布式音频设备NetworkID。
     * @param devId 分布式音频设备的端口ID。
     * @param event 通知事件类型(如焦点事件，音量事件) 
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    NotifyEvent([in] String adpName, [in] int devId, [in] struct DAudioEvent event);
    /**
     * @brief 向分布式音频设备写入播放流。
     *
     * @param adpName 分布式音频设备NetworkID。
     * @param devId 分布式音频设备的端口ID。
     * @param data 音频流数据。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    WriteStreamData([in] String adpName, [in] int devId, [in] struct AudioData data);
    /**
     * @brief 向分布式音频设备读取录制流。
     *
     * @param adpName 分布式音频设备NetworkID。
     * @param devId 分布式音频设备的端口ID。
     * @param data 音频流数据。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    ReadStreamData([in] String adpName, [in] int devId, [out] struct AudioData data);
    /**
     * @brief 获取当前读写的帧数及时间戳
     *
     * @param adpName 分布式音频设备NetworkID。
     * @param devId 分布式音频设备的端口ID。
     * @param frames 获取的当前帧数及时间戳。
     * @param time 当前时间戳。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    ReadMmapPosition([in] String adpName, [in] int devId, [out] unsigned long frames, [out] struct CurrentTime time);
    /**
     * @brief 刷新共享内存信息
     *
     * @param adpName 分布式音频设备NetworkID。
     * @param devId 分布式音频设备的端口ID。
     * @param fd 共享内存对应文件描述符。
     * @param ashmemLength 共享内存总字节数。
     * @param lengthPerTrans 每次传输的字节数。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    RefreshAshmemInfo([in] String adpName, [in] int devId, [in] FileDescriptor fd, [in] int ashmemLength, [in] int lengthPerTrans);
}
/** @} */