# C/C++ API

- [OpenHarmony注释规范--注释概述](#OpenHarmony注释规范-注释概述)
- [OpenHarmony注释规范--定义分组注释(C/C++）](#OpenHarmony注释规范-定义分组注释-c-c-)
  - [注释模板](#注释模板)
  - [注释样例](#注释样例)
  - [注释规范](#注释规范)
- [OpenHarmony注释规范--文件File注释（C/C++）](#OpenHarmony注释规范-文件file注释-c-c-)
  - [注释模板](#注释模板)
  - [注释样例](#注释样例)
  - [注释规范](#注释规范)
- [OpenHarmony注释规范--普通常量和变量注释（C/C++）](#OpenHarmony注释规范-普通常量和变量注释-c-c-)
  - [注释模板](#注释模板)
  - [注释样例](#注释样例)
  - [注释规范](#注释规范)
- [OpenHarmony注释规范--枚举Enum注释（C/C++）](#OpenHarmony注释规范-枚举enum注释-c-c-)
  - [注释模板](#注释模板)
  - [注释样例](#注释样例)
  - [注释规范](#注释规范)
- [OpenHarmony注释规范--结构体Struct和联合体Union注释（C/C++）](#OpenHarmony注释规范-结构体struct和联合体union注释-c-c-)
  - [注释模板](#注释模板)
  - [注释样例](#注释样例)
  - [注释规范](#注释规范)
- [OpenHarmony注释规范--函数Function注释（C/C++）](#OpenHarmony注释规范-函数function注释-c-c-)
  - [注释模板](#注释模板)
  - [注释样例](#注释样例)
  - [注释规范](#注释规范)
- [OpenHarmony注释规范--类型定义typedef和宏定义define注释（C/C++）](#OpenHarmony注释规范-类型定义typedef和宏定义define注释-c-c-)
  - [类型定义typedef](#类型定义typedef)
  - [宏定义define](#宏定义define)

## OpenHarmony注释规范--注释概述

C/C++接口要求在.h文件中进行注释，然后借助Doxygen工具提取注释内容，生成API参考。C语言注释分为两种：

1. 传统风格的注释，一般用/\* \*/或//来标记，不用于生成文档。

2. 能用Doxygen生成文档的注释，我们把它叫做文档注释，一般用/\*\* \*/来标记文档注释。

本指导介绍文件、常量、变量、枚举、结构体、联合、函数、typedef、define和示例代码的注释规范，包括注释的内容上的规范和格式上的规范。

> ![icon-note.gif](public_sys-resources/icon-note.gif) **说明：**
> - 本指导仅针对需要对外部开发者暴露的信息，此类信息使用能用Doxygen生成文档的注释格式/\*\* \*/，目的是可以在注释中提取API参考。
> 
> - 不需要对外开发暴露的信息，请使用传统风格注释格式/\* \*/或//，并遵守编程规范的要求。“.h”会作为交付件发布出去，所以代码以及注释都要合规。例如，对于copyright、日期等信息等信息，不需要在API参考中体现，但需要在代码中写作，请采用/\* \*/格式在文件顶部进行注释。
> 
> - 本规范仅适用OpenHarmony库。如果是标准的语言库或三方定义的api，使用对应的规范； 系统需要提供相应的外部语言库版本等基本信息查询的api接口。
> 
> - \@since在文件File、定义分组、类的注释中必选，其余元素按条件必选，填写场景为：File、分组或类为1.0版本，但是其包含的元素（例如函数、变量等）为1.1版本新增，则此时必须对该元素标记\@since。\@verison为可选，如果有库版本号，需要填写\@verison。
> 
> - 模块的版本号实现上必须提供可查询版本号getversion()的api接口。
> 
> - 每个库至少定义一个模块，不同库不能定义为相同模块。


## OpenHarmony注释规范--定义分组注释(C/C++）

### 注释规范

- 如果一个头文件中函数需要细分成不同的类别，并且需要对应到API参考所在目录，则可以在文件注释后添加分组定义。
- \@addtogroup后接模块名。如果需要将头文件中具体的函数划分到该模块中，则在具体函数的函数前使\@addtogroup标记，并使用“\@{”、“\@}”将该函数包起来。
- \@since后接引入OS的版本号。
- \@version后接该库自身版本号。

### 注释模板

```
/**
 * @addtogroup 模块名
 * @{
 * 
 * @brief 一句话描述该模块的作用。
 * 
 * （可选）详细描述该模块的主要功能、使用场景和使用建议。
 *
 * @since OS的版本号
 * @version 库的版本号
 */
...
/** @} */
```


### 注释样例

```
/**
 * @addtogroup bitmap
 * @{
 *
 * @brief ANativeWindow represents the producer end of an image queue.
 *
 * It is the C counterpart of the android.view.Surface object in Java,
 * and can be converted both ways. Depending on the consumer, images
 * submitted to ANativeWindow can be shown on the display or sent to
 * other consumers, such as video encoders.
 *
 * @since 1
*/
intAndroidBitmap_unlock(JNIEnv*env, jobjectjbitmap);
...
/** @} */

```


## OpenHarmony注释规范--文件File注释（C/C++）

### 注释规范

- 文件的注释放在代码前。
- 一行注释不能超过79列，即一行不超过80字节，直接回车换行。
- \@file后接文件名，文件名需要和实际文件命名保持一致。
- 详细描述中，如果有多个段落，每段必须以“\n”结束。
- 注意区分其种的中文和英文标点符号。
- 中间的注释”\*”前后需要有一个空格。
- \@since后接API LEVEL（开发内部对齐）。
- \@version后接该模块/库的版本号。

### 注释模板

```
/**
 * @file 文件名
 *
 * @brief 一句话描述该头文件的作用。
 *
 * （可选）详细描述该头文件的主要功能、使用场景和使用建议。
 * 
 * @since OS的版本号
 * @version 库的版本号
 */
```


### 注释样例

```
/**
 * @file bitmap.h
 *
 * @brief API for accessing a native window.
 * 
 * @since 1
 * @version 1
 */
```


## OpenHarmony注释规范--普通常量和变量注释（C/C++）

### 注释规范

- 简要描述和详细描述之间需要空行。
- 一行注释不要超过79列，即一行不超过80字节，直接回车续行。
- 详细描述中，如果有多个段落，每段必须以“\n”结束。

### 注释模板

```
/**
 * @brief 一句话描述该常量/变量的含义。
 * 
 * （可选）详细描述该常量/变量的作用、使用限制和建议、取值范围，以及取到边界值、非法值的后果。
 */
<const> int gExample;
```


### 注释样例

```
/**
 * @brief The name of the function that NativeInstance looks for when launching its
 * native code.
 *
 * This is the default function that is used, you can specify "android.app.func_name"
 * string meta-data in your manifest to use a different function.
 */
extern ANativeActivity_createFunc ANativeActivity_onCreate;
```


## OpenHarmony注释规范--枚举Enum注释（C/C++）

### 注释规范

- 针对枚举的详细描述中，如果有多个段落，每段必须以“\n”结束。
- 针对枚举值，要对每一个枚举值的含义进行说明。
- 一行注释不要超过79列，即一行不超过80字节，直接回车续行。

### 注释模板

```
/**
 * @brief 一句话描述该枚举的作用。
 * 
 * （可选）详细描述该枚举的主要功能、使用场景和使用建议。
 */
enum EnumName 
{
/** 描述枚举值1的含义 */
EnumMermber1 = 1, 
/** 描述枚举值2的含义 */
EnumMermber2, 
/** 描述枚举值3的含义 */
EnumMermber3
}; 
```


### 注释样例

```
/**
 * @brief Flags for ANativeActivity_showSoftInput; see the Java InputMethodManager
 * API for documentation.
 */
enum {
/**
 * Implicit request to show the input window, not as the result
 * of a direct request by the user.
 */
ANATIVEACTIVITY_SHOW_SOFT_INPUT_IMPLICIT = 0x0001,
/**
 * The user has forced the input method open (such as by
 * long-pressing menu) so it should not be closed until they
 * explicitly do so.
 */
ANATIVEACTIVITY_SHOW_SOFT_INPUT_FORCED = 0x0002,
};
```


## OpenHarmony注释规范--结构体Struct和联合体Union注释（C/C++）

### 注释规范

- 针对结构体或联合体的详细描述中，如果有多个段落，每段必须以“\n”结束。
- 针对成员，要对每一个成员的含义进行说明。
- 一行注释不要超过79列，即一行不超过80字节，直接回车续行。

### 注释模板

```
/**
 * @brief 一句话描述该结构体或联合体的作用。
 *
 * （可选）详细描述该结构体或联合体的作用、使用场合和建议等。
 */
Typedef struct StructName
{ 
/** 描述成员1的含义 */
unsigned long StructMember1;
/** 描述成员2的含义 */
unsigned long StructMember2;
};
```


### 注释样例

```
/**
 * @brief These are the callbacks the framework makes into a native application.
 * 
 * All of these callbacks happen on the main thread of the application.
 * By default, all callbacks are NULL; set to a pointer to your own function
 * to have it called.
 */
typedef struct ANativeActivityCallbacks {
/**
 * NativeActivity has started. See Java documentation for Activity.onStart()
 * for more information.
 */
void (*onStart)(ANativeActivity*activity);
/**
 * NativeActivity has resumed. See Java documentation for Activity.onResume()
 * for more information.
 */
void (*onResume)(ANativeActivity*activity);
} ANativeActivityCallbacks;
```


## OpenHarmony注释规范--函数Function注释（C/C++）

### 注释规范

- 简要描述和详细描述之间需要空行。

- 详细描述中，如果有多个段落，每段必须以“\n”结束。

- 文档标记和详细描述之间需要空行。

- 在注释过程中涉及到到另外一个接口，可以在该接口前加“\#”或用{\@link}包起来，则在生成的手册中将自动添加到该接口的链接。如“\@param param1 该参数可以通过 \#fuction1 获取”或“\@param param1 该参数可以通过 {\@link fuction1} 获取”。但是\@see后所写的函数不需要，工具会自动生成链接。

- 函数注释中涉及的文档标记及用法如下：

  | 标记名称 | 标记说明                                                     |
  | -------- | ------------------------------------------------------------ |
  | \@param  | 格式为：\@param+参数名+参数描述，一个参数使用一个\@param标记。参数描述需要包含如下信息：<br/>- 参数的作用、使用限制和建议<br/>- 参数的取值范围，以及取到边界值、非法值得后果<br/>- 如果存在参数设置方面的建议值和经验值，请描述 |
  | \@return | 格式为：\@return+返回值描述。返回值描述介绍返回值的含义，没有返回值，则请删除该标记。 |
  | \@see    | 用于标记与该方法相关联的方法：功能相近或者存在关系。         |

### 注释模板

```
/**
 * @brief 一句话描述该函数的作用。
 *
 * 详细描述该函数的主要功能、使用场景和使用建议。
 *
 * @param （可选）后接参数名和参数描述，一个参数使用一个@param标记。参数描述写作要点：1.参数的作用、使用限制和建议；2.参数的取值范围，以及取到边界值、非法值的后果；3.如果存在参数设置方面的建议值或经验值，请描述。如果该方法没有参数，则请删除该标记
 * @return 返回值描述。返回值描述介绍返回值的含义，没有返回值，则请删除该标记。
 * @see （可选）当存在与该方法相关联的函数时（功能相近或者存在关系），可以通过@see建立到参考方法的链接。如果不涉及，则请删除该标记
 */
int function (int param1, int param2);
```


### 注释样例

```
/**
 * @brief Allocate dynamic memory.
 *
 * This API is used to allocate a memory block of which the size is specified and update module mem used.
 *
 * @param  pool      Pointer to the memory pool that contains the memory block to be allocated.
 * @param  size      Size of the memory block to be allocated (unit: byte).
 * @param  moduleID  module ID (0~MODULE_MAX).
 * @return When memory is successfully allocated with the starting address of the allocated memory block,it returns VOID*.
                   When memory fails to be allocated,it rerurns NULL.
 * @see LOS_MemRealloc | LOS_MemAllocAlign | LOS_MemFree
 */
extern VOID *LOS_MemMalloc(VOID *pool, UINT32 size, UINT32 moduleID);
```


## OpenHarmony注释规范--类型定义typedef和宏定义define注释（C/C++）


### 类型定义typedef

- typdef定义的类型为常量或变量，则采用普通常量和变量注释（C\C++）规范。

- typdef定义的类型为枚举，则采用枚举Enum注释（C\C++）规范。

- typdef定义的类型为结构体或联合体，则采用结构体Struct和联合体Union注释（C\C++）规范。

- typdef定义的类型为函数，则采用函数Function注释（C\C++）规范。


### 宏定义define

- define定义的类型为常量或变量，则采用普通常量和变量注释（C\C++）规范。

- define定义的类型为函数，则采用函数Function注释（C\C++）规范。
