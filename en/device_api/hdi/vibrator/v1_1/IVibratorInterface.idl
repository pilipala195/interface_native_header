/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Vibrator
 * @{
 *
 * @brief Provides APIs for vibrator services to access the vibrator driver.
 *
 * After obtaining a driver object or agent, a vibrator service starts or stops the vibrator or setting
 * intensity and frequency, using the APIs provided by the driver object or agent.
 *
 * @since 3.2
 * @version 1.1
 */

/**
 * @file IVibratorInterface.idl
 *
 * @brief Declares common APIs in the vibrator module. The APIs can be used to obtain information about all the vibrators
 * that support intensity setting and start the vibrator according to the passed vibration effect.
 *
 * @since 3.2
 * @version 1.1
 */

/**
 * @brief Defines the package path of the Vibrator module APIs.
 *
 * @since 3.2
 * @version 1.1
 */
package ohos.hdi.vibrator.v1_1;

import ohos.hdi.vibrator.v1_1.VibratorTypes;

/**
 * @brief Defines the interface for performing basic operations on vibrators.
 *
 * The operation includes starting a vibrator with the specified vibration mode and effect, and stopping the vibrator.
 * @since 3.2
 * @version 1.1
 */
interface IVibratorInterface {
    /**
     * @brief Controls the vibrator to perform a one-shot vibration that lasts for a given duration.
     *
     * One-shot vibration is mutually exclusive with periodic vibration. Before using one-shot vibration,
     * exit periodic vibration.
     *
     * @param duration Indicates the duration that the one-shot vibration lasts, in milliseconds.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a negative value if the operation fails.
     *
     * @since 2.2
     * @version 1.0
     */
    StartOnce([in] unsigned int duration);

    /**
     * @brief Controls the vibrator to perform a periodic vibration with the preset effect.
     *
     * One-shot vibration is mutually exclusive with periodic vibration. Before using periodic vibration,
     * exit one-shot vibration.
     *
     * @param effectType Indicates the preset effect type. It is recommended that the
     * maximum length be 64 bytes.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a negative value if the operation fails.
     *
     * @since 2.2
     * @version 1.0
     */
    Start([in] String effectType);

    /**
     * @brief Stops the vibration.
     *
     * Before the vibrator starts, it must stop vibrating in any mode. This function can be used
     * after the vibrating process.
     *
     * @param mode Indicates the vibration mode, which can be one-shot or periodic. For details,
     * see {@link HdfVibratorMode}.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a negative value if the operation fails.
     *
     * @since 2.2
     * @version 1.0
     */
    Stop([in] enum HdfVibratorMode mode);

    /**
     * @brief Obtains information about all the vibrators that support intensity and frequency setting in the system.
     *
     * @param vibratorInfo Indicates the vibrator information. For details, see {@Link HdfVibratorInfo}.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a negative value if the operation fails.
     *
     * @since 3.2
     * @version 1.1
     */
    GetVibratorInfo([out] struct HdfVibratorInfo[] vibratorInfo);

    /**
     * @brief Starts the vibrator with the specified vibration effect.
     *
     * @param duration Indicates the duration that the vibration lasts, in milliseconds.
     * @param intensity Indicates the vibration intensity.
     * @param frequency Indicates the vibration frequency.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns <b>-1</b> if the duration setting is not supported.
     * @return Returns <b>-2</b> if the intensity setting is not supported.
     * @return Returns <b>-3</b> if the frequency setting is not supported.
     *
     * @since 3.2
     * @version 1.1
     */
    EnableVibratorModulation([in] unsigned int duration, [in] int intensity, [in] int frequency);
}
/** @} */
