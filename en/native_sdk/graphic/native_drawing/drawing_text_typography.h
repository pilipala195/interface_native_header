/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_TEXT_TYPOGRAPHY_H
#define C_INCLUDE_DRAWING_TEXT_TYPOGRAPHY_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_text_typography.h
 *
 * @brief Declares the functions related to the typography in the drawing module.
 *
 * File to include: native_drawing/drawing_text_typography.h
 * @library libnative_drawing.so
 * @since 8
 * @version 1.0
 */

#include "drawing_canvas.h"
#include "drawing_color.h"
#include "drawing_text_declaration.h"
#include "drawing_types.h"

#include "stdint.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the text directions.
 */
enum OH_Drawing_TextDirection {
    /** Right to left (RTL). */
    TEXT_DIRECTION_RTL,
    /** Left to right (LTR). */
    TEXT_DIRECTION_LTR,
};

/**
 * @brief Enumerates the text alignment modes.
 */
enum OH_Drawing_TextAlign {
    /** Left-aligned. */
    TEXT_ALIGN_LEFT,
    /** Right-aligned. */
    TEXT_ALIGN_RIGHT,
    /** Center-aligned. */
    TEXT_ALIGN_CENTER,
    /**
     * Justified, which means that each line (except the last line) is stretched so that every line has equal width,
     * and the left and right margins are straight.
     */
    TEXT_ALIGN_JUSTIFY,
    /**
     * <b>TEXT_ALIGN_START</b> achieves the same effect as <b>TEXT_ALIGN_LEFT</b>
     * when <b>OH_Drawing_TextDirection</b> is <b>TEXT_DIRECTION_LTR</b>;
     * it achieves the same effect as <b>TEXT_ALIGN_RIGHT</b>
     * when <b>OH_Drawing_TextDirection</b> is <b>TEXT_DIRECTION_RTL</b>.
     */
    TEXT_ALIGN_START,
    /**
     * <b>TEXT_ALIGN_END</b> achieves the same effect as <b>TEXT_ALIGN_RIGHT</b>
     * when <b>OH_Drawing_TextDirection</b> is <b>TEXT_DIRECTION_LTR</b>;
     * it achieves the same effect as <b>TEXT_ALIGN_LEFT</b>
     * when <b>OH_Drawing_TextDirection</b> is <b>TEXT_DIRECTION_RTL</b>.
     */
    TEXT_ALIGN_END,
};

/**
 * @brief Enumerates the font weights.
 */
enum OH_Drawing_FontWeight {
    /** Thin. */
    FONT_WEIGHT_100,
    /** Extra-light. */
    FONT_WEIGHT_200,
    /** Light. */
    FONT_WEIGHT_300,
    /** Normal/Regular. */
    FONT_WEIGHT_400,
    /** Medium. */
    FONT_WEIGHT_500,
    /** Semi-bold. */
    FONT_WEIGHT_600,
    /** Bold. */
    FONT_WEIGHT_700,
    /** Extra-bold. */
    FONT_WEIGHT_800,
    /** Black. */
    FONT_WEIGHT_900,
};

/**
 * @brief Enumerates the text baselines.
 */
enum OH_Drawing_TextBaseline {
    /** Alphabetic, where the letters in alphabets like English sit on. */
    TEXT_BASELINE_ALPHABETIC,
    /** Ideographic. The baseline is at the bottom of the text area. */
    TEXT_BASELINE_IDEOGRAPHIC,
};

/**
 * @brief Enumerates the text decorations.
 */
enum OH_Drawing_TextDecoration {
    /** No decoration. */
    TEXT_DECORATION_NONE = 0x0,
    /** A underline is used for decoration. */
    TEXT_DECORATION_UNDERLINE = 0x1,
    /** An overline is used for decoration. */
    TEXT_DECORATION_OVERLINE = 0x2,
    /** A strikethrough is used for decoration. */
    TEXT_DECORATION_LINE_THROUGH = 0x4,
};

/**
 * @brief Enumerates the font styles.
 */
enum OH_Drawing_FontStyle {
    /** Normal style. */
    FONT_STYLE_NORMAL,
    /** Italic. */
    FONT_STYLE_ITALIC,
};

/**
 * @brief Enumerates the vertical alignment modes of placeholders.
 *
 * @since 11
 * @version 1.0
 */
typedef enum {
    /** Aligned to the baseline. */
    ALIGNMENT_OFFSET_AT_BASELINE,
    /** Aligned above the baseline. */
    ALIGNMENT_ABOVE_BASELINE,
    /** Aligned below the baseline. */
    ALIGNMENT_BELOW_BASELINE,
    /** Aligned to the top of the row box. */
    ALIGNMENT_TOP_OF_ROW_BOX,
    /** Aligned to the bottom of the row box. */
    ALIGNMENT_BOTTOM_OF_ROW_BOX,
    /** Aligned to the center of the row box. */
    ALIGNMENT_CENTER_OF_ROW_BOX,
} OH_Drawing_PlaceholderVerticalAlignment;

/**
 * @brief Describes a placeholder that acts as a span.
 *
 * @since 11
 * @version 1.0
 */
typedef struct {
    /** Width of the placeholder. */
    double width;
    /** Height of the placeholder. */
    double height;
    /** Alignment mode of the placeholder. */
    OH_Drawing_PlaceholderVerticalAlignment alignment;
    /** Baseline of the placeholder. */
    OH_Drawing_TextBaseline baseline;
    /** Baseline offset of the placeholder. */
    double baselineOffset;
} OH_Drawing_PlaceholderSpan;

/**
 * @brief Enumerates the text decoration styles.
 *
 * @since 11
 * @version 1.0
 */
typedef enum {
    /** Solid style. */
    TEXT_DECORATION_STYLE_SOLID,
    /** Double style. */
    TEXT_DECORATION_STYLE_DOUBLE,
    /** Dotted style. */
    TEXT_DECORATION_STYLE_DOTTED,
    /** Dashed style. */
    TEXT_DECORATION_STYLE_DASHED,
    /** Wavy style. */
    TEXT_DECORATION_STYLE_WAVY,
} OH_Drawing_TextDecorationStyle;

/**
 * @brief Enumerates the ellipsis styles.
 *
 * @since 11
 * @version 1.0
 */
typedef enum {
    /** Places the ellipsis in the text header. */
    ELLIPSIS_MODAL_HEAD = 0,
    /** Places the ellipsis in the middle of the text. */
    ELLIPSIS_MODAL_MIDDLE = 1,
    /** Places the ellipsis at the end of the text. */
    ELLIPSIS_MODAL_TAIL = 2,
} OH_Drawing_EllipsisModal;

/**
 * @brief Enumerates the text break strategies.
 *
 * @since 11
 * @version 1.0
 */
typedef enum {
    /** Each line is filled as much as possible during line break. */
    BREAK_STRATEGY_GREEDY = 0,
    /** Text continuity is preferentially considered during line break. */
    BREAK_STRATEGY_HIGH_QUALITY = 1,
    /** Line breaks are performed at the word boundary. */
    BREAK_STRATEGY_BALANCED = 2,
} OH_Drawing_BreakStrategy;

/**
 * @brief Enumerates the word break types.
 *
 * @since 11
 * @version 1.0
 */
typedef enum {
    /** Normal mode. */
    WORD_BREAK_TYPE_NORMAL = 0,
    /** Breaks the words at any character to prevent overflow. */
    WORD_BREAK_TYPE_BREAK_ALL = 1,
    /** Breaks the words at arbitrary points to prevent overflow. */
    WORD_BREAK_TYPE_BREAK_WORD = 2,
} OH_Drawing_WordBreakType;

/**
 * @brief Enumerates the rectangle height styles.
 *
 * @since 11
 * @version 1.0
 */
typedef enum {
    /** Tight style. */
    RECT_HEIGHT_STYLE_TIGHT,
    /** Maximum style. */
    RECT_HEIGHT_STYLE_MAX,
    /** Middle style that includes the line spacing. */
    RECT_HEIGHT_STYLE_INCLUDELINESPACEMIDDLE,
    /** Top style that includes the line spacing. */
    RECT_HEIGHT_STYLE_INCLUDELINESPACETOP,
    /** Bottom style that includes the line spacing. */
    RECT_HEIGHT_STYLE_INCLUDELINESPACEBOTTOM,
    /** Structure style. */
    RECT_HEIGHT_STYLE_STRUCT,
} OH_Drawing_RectHeightStyle;

/**
 * @brief Enumerates the rectangle width styles.
 *
 * @since 11
 * @version 1.0
 */
typedef enum {
    */Tight style. */
    RECT_WIDTH_STYLE_TIGHT,
    */Maximum style. */
    RECT_WIDTH_STYLE_MAX,
} OH_Drawing_RectWidthStyle;

/**
 * @brief Creates an <b>OH_Drawing_TypographyStyle</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the <b>OH_Drawing_TypographyStyle</b> object created.
 * @since 8
 * @version 1.0
 */
OH_Drawing_TypographyStyle* OH_Drawing_CreateTypographyStyle(void);

/**
 * @brief Destroys an <b>OH_Drawing_TypographyStyle</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an <b>OH_Drawing_TypographyStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypographyStyle}.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_DestroyTypographyStyle(OH_Drawing_TypographyStyle*);

/**
 * @brief Sets the text direction.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an <b>OH_Drawing_TypographyStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypographyStyle}.
 * @param int Text direction. For details about the available options, see {@link OH_Drawing_TextDirection}.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextDirection(OH_Drawing_TypographyStyle*, int /* OH_Drawing_TextDirection */);

/**
 * @brief Sets the text alignment mode.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an <b>OH_Drawing_TypographyStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypographyStyle}.
 * @param int Text alignment mode. For details about the available options, see {@link OH_Drawing_TextAlign}.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextAlign(OH_Drawing_TypographyStyle*, int /* OH_Drawing_TextAlign */);

/**
 * @brief Sets the maximum number of lines in the text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an <b>OH_Drawing_TypographyStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypographyStyle}.
 * @param int Maximum number of lines.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextMaxLines(OH_Drawing_TypographyStyle*, int /* maxLines */);

/**
 * @brief Creates an <b>OH_Drawing_TextStyle</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the <b>OH_Drawing_TextStyle</b> object created.
 * @since 8
 * @version 1.0
 */
OH_Drawing_TextStyle* OH_Drawing_CreateTextStyle(void);

/**
 * @brief Destroys an <b>OH_Drawing_TextStyle</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTextStyle}.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_DestroyTextStyle(OH_Drawing_TextStyle*);

/**
 * @brief Sets the text color.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTextStyle}.
 * @param uint32_t Color.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleColor(OH_Drawing_TextStyle*, uint32_t /* color */);

/**
 * @brief Sets the font size.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTextStyle}.
 * @param double Font size.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontSize(OH_Drawing_TextStyle*, double /* fontSize */);

/**
 * @brief Sets the font weight.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTextStyle}.
 * @param int Font weight.
 * For details about the available options, see {@link OH_Drawing_FontWeight}.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontWeight(OH_Drawing_TextStyle*, int /* OH_Drawing_FontWeight */);

/**
 * @brief Sets the text baseline.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTextStyle}.
 * @param int Text baseline. For details about the available options, see {@link OH_Drawing_TextBaseline}.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleBaseLine(OH_Drawing_TextStyle*, int /* OH_Drawing_TextBaseline */);

/**
 * @brief Sets the text decoration.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTextStyle}.
 * @param int Text decoration. For details about the available options, see {@link OH_Drawing_TextDecoration}.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleDecoration(OH_Drawing_TextStyle*, int /* OH_Drawing_TextDecoration */);

/**
 * @brief Sets the color for the text decoration.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTextStyle}.
 * @param uint32_t Color.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleDecorationColor(OH_Drawing_TextStyle*, uint32_t /* color */);

/**
 * @brief Sets the font height.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTextStyle}.
 * @param double Font height.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontHeight(OH_Drawing_TextStyle*, double /* fontHeight */);

/**
 * @brief Sets the font families.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTextStyle}.
 * @param int Number of font families.
 * @param char Pointer to the font families.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontFamilies(OH_Drawing_TextStyle*,
    int /* fontFamiliesNumber */, const char* fontFamilies[]);

/**
 * @brief Sets the font style.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTextStyle}.
 * @param int Font style. For details about the available options, see {@link OH_Drawing_FontStyle}.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontStyle(OH_Drawing_TextStyle*, int /* OH_Drawing_FontStyle */);

/**
 * @brief Sets the locale.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTextStyle}.
 * @param char Pointer to the locale.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleLocale(OH_Drawing_TextStyle*, const char*);

/**
 * @brief Creates an <b>OH_Drawing_TypographyCreate</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an <b>OH_Drawing_TypographyStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypographyStyle}.
 * @param OH_Drawing_FontCollection Pointer to an <b>OH_Drawing_FontCollection</b> object, which is obtained by
 * {@link OH_Drawing_CreateFontCollection}.
 * @return Returns the pointer to the <b>OH_Drawing_TypographyCreate</b> object created.
 * @since 8
 * @version 1.0
 */
OH_Drawing_TypographyCreate* OH_Drawing_CreateTypographyHandler(OH_Drawing_TypographyStyle*,
    OH_Drawing_FontCollection*);

/**
 * @brief Destroys an <b>OH_Drawing_TypographyCreate</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate Pointer to an <b>OH_Drawing_TypographyCreate</b> object. The pointer is obtained
 * by {@link OH_Drawing_CreateTypographyHandler}.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_DestroyTypographyHandler(OH_Drawing_TypographyCreate*);

/**
 * @brief Sets the text style.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate Pointer to an <b>OH_Drawing_TypographyCreate</b> object. The pointer is obtained
 * by {@link OH_Drawing_CreateTypographyHandler}.
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTextStyle}.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyHandlerPushTextStyle(OH_Drawing_TypographyCreate*, OH_Drawing_TextStyle*);

/**
 * @brief Sets the text content.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate Pointer to an <b>OH_Drawing_TypographyCreate</b> object. The pointer is obtained
 * by {@link OH_Drawing_CreateTypographyHandler}.
 * @param char Pointer to the text content.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyHandlerAddText(OH_Drawing_TypographyCreate*, const char*);

/**
 * @brief Removes the topmost style in the stack, leaving the remaining styles in effect.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate Pointer to an <b>OH_Drawing_TypographyCreate</b> object. The pointer is obtained
 * by {@link OH_Drawing_CreateTypographyHandler}.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyHandlerPopTextStyle(OH_Drawing_TypographyCreate*);

/**
 * @brief Creates an <b>OH_Drawing_Typography</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate Pointer to an <b>OH_Drawing_TypographyCreate</b> object. The pointer is obtained
 * by {@link OH_Drawing_CreateTypographyHandler}.
 * @return Returns the pointer to the <b>OH_Drawing_Typography</b> object created.
 * @since 8
 * @version 1.0
 */
OH_Drawing_Typography* OH_Drawing_CreateTypography(OH_Drawing_TypographyCreate*);

/**
 * @brief Destroys an <b>OH_Drawing_Typography</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypography}.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_DestroyTypography(OH_Drawing_Typography*);

/**
 * @brief Lays out the typography.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypography}.
 * @param double Maximum text width.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyLayout(OH_Drawing_Typography*, double /* maxWidth */);

/**
 * @brief Paints text on the canvas.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypography}.
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object, which is obtained by
 * {@link OH_Drawing_CanvasCreate()}.
 * @param double X coordinate.
 * @param double Y coordinate.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyPaint(OH_Drawing_Typography*, OH_Drawing_Canvas*,
    double /* potisionX */, double /* potisionY */);

/**
 * @brief Obtains the maximum width.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypography}.
 * @return Returns the maximum width.
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetMaxWidth(OH_Drawing_Typography*);

/**
 * @brief Obtains the height.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypography}.
 * @return Returns the height.
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetHeight(OH_Drawing_Typography*);

/**
 * @brief Obtains the width of the longest line. You are advised to round up the return value in actual use.
 * When the text content is empty, the minimum float value,
 * that is, -340282346638528859811704183484516925440.000000, is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypography}.
 * @return Returns the width of the longest row.
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetLongestLine(OH_Drawing_Typography*);

/**
 * @brief Obtains the minimum intrinsic width.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypography}.
 * @return Returns the minimum intrinsic width.
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetMinIntrinsicWidth(OH_Drawing_Typography*);

/**
 * @brief Obtains the maximum intrinsic width.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypography}.
 * @return Returns the maximum intrinsic width.
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetMaxIntrinsicWidth(OH_Drawing_Typography*);

/**
 * @brief Obtains the alphabetic baseline.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypography}.
 * @return Returns the alphabetic baseline.
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetAlphabeticBaseline(OH_Drawing_Typography*);

/**
 * @brief Obtains the ideographic baseline.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypography}.
 * @return Returns the ideographic baseline.
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetIdeographicBaseline(OH_Drawing_Typography*);

/**
 * @brief Adds a placeholder.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate Pointer to an <b>OH_Drawing_TypographyCreate</b> object. The pointer is obtained
 * by {@link OH_Drawing_CreateTypographyHandler}.
 * @param OH_Drawing_PlaceholderSpan Pointer to an <b>OH_Drawing_PlaceholderSpan</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_TypographyHandlerAddPlaceholder(OH_Drawing_TypographyCreate*, OH_Drawing_PlaceholderSpan*);

/**
 * @brief Checks whether the maximum number of lines is exceeded.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypography}.
 * @return Returns <b>true</b> if the maximum number of lines is exceeded; returns <b>false</b> otherwise.
 * @since 11
 * @version 1.0
 */
bool OH_Drawing_TypographyDidExceedMaxLines(OH_Drawing_Typography*);

/**
 * @brief Obtains text boxes in a given range.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypography}.
 * @param size_t Start position.
 * @param size_t End position.
 * @param OH_Drawing_RectHeightStyle Height style. For details about the available options,
 * see {@link OH_Drawing_RectHeightStyle}.
 * @param OH_Drawing_RectWidthStyle Width style. For details about the available options,
 * see {@link OH_Drawing_RectWidthStyle}.
 * @return Returns the {@link OH_Drawing_TextBox} struct that holds the text boxes.
 * @since 11
 * @version 1.0
 */
OH_Drawing_TextBox* OH_Drawing_TypographyGetRectsForRange(OH_Drawing_Typography*,
    size_t, size_t, OH_Drawing_RectHeightStyle, OH_Drawing_RectWidthStyle);

/**
 * @brief Obtains text boxes for placeholders.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypography}.
 * @return Returns the {@link OH_Drawing_TextBox} struct that holds the text boxes.
 * @since 11
 * @version 1.0
 */
OH_Drawing_TextBox* OH_Drawing_TypographyGetRectsForPlaceholders(OH_Drawing_Typography*);

/**
 * @brief Obtains the left position of a text box.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox Pointer to an <b>OH_Drawing_TextBox</b> object, which is obtained by
 * {@link OH_Drawing_TypographyGetRectsForRange} or {@link OH_Drawing_TypographyGetRectsForPlaceholders}.
 * @param int Index of the text box.
 * @return Returns the left position.
 * @since 11
 * @version 1.0
 */
float OH_Drawing_GetLeftFromTextBox(OH_Drawing_TextBox*, int);

/**
 * @brief Obtains the right position of a text box.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox Pointer to an <b>OH_Drawing_TextBox</b> object, which is obtained by
 * {@link OH_Drawing_TypographyGetRectsForRange} or {@link OH_Drawing_TypographyGetRectsForPlaceholders}.
 * @param int Index of the text box.
 * @return Returns the right position.
 * @since 11
 * @version 1.0
 */
float OH_Drawing_GetRightFromTextBox(OH_Drawing_TextBox*, int);

/**
 * @brief Obtains the top position of a text box.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox Pointer to an <b>OH_Drawing_TextBox</b> object, which is obtained by
 * {@link OH_Drawing_TypographyGetRectsForRange} or {@link OH_Drawing_TypographyGetRectsForPlaceholders}.
 * @param int Index of the text box.
 * @return Returns the top position.
 * @since 11
 * @version 1.0
 */
float OH_Drawing_GetTopFromTextBox(OH_Drawing_TextBox*, int);

/**
 * @brief Obtains the bottom position of a text box.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox Pointer to an <b>OH_Drawing_TextBox</b> object, which is obtained by
 * {@link OH_Drawing_TypographyGetRectsForRange} or {@link OH_Drawing_TypographyGetRectsForPlaceholders}.
 * @param int Index of the text box.
 * @return Returns the bottom position.
 * @since 11
 * @version 1.0
 */
float OH_Drawing_GetBottomFromTextBox(OH_Drawing_TextBox*, int);

/**
 * @brief Obtains the text direction of a text box.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox Pointer to an <b>OH_Drawing_TextBox</b> object, which is obtained by
 * {@link OH_Drawing_TypographyGetRectsForRange} or {@link OH_Drawing_TypographyGetRectsForPlaceholders}.
 * @param int Index of the text box.
 * @return Returns the text direction.
 * @since 11
 * @version 1.0
 */
int OH_Drawing_GetTextDirectionFromTextBox(OH_Drawing_TextBox*, int);

/**
 * @brief Obtains the number of text boxes.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox Pointer to an <b>OH_Drawing_TextBox</b> object, which is obtained by
 * {@link OH_Drawing_TypographyGetRectsForRange} or {@link OH_Drawing_TypographyGetRectsForPlaceholders}.
 * @return Returns the number of text boxes.
 * @since 11
 * @version 1.0
 */
size_t OH_Drawing_GetSizeOfTextBox(OH_Drawing_TextBox*);

/**
 * @brief Obtains the position and affinity of the glyph at the given coordinates.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypography}.
 * @param double X coordinate.
 * @param double Y coordinate.
 * @return Returns the {@link OH_Drawing_PositionAndAffinity} struct that holds the position and affinity of the glyph.
 * @since 11
 * @version 1.0
 */
OH_Drawing_PositionAndAffinity* OH_Drawing_TypographyGetGlyphPositionAtCoordinate(OH_Drawing_Typography*,
    double, double);

/**
 * @brief Obtains the position and affinity of the glyph cluster to which the glyph at the given coordinates belongs.
 * The glyph cluster is a container that holds one or more glyphs.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypography}.
 * @param double X coordinate.
 * @param double Y coordinate.
 * @return Returns the {@link OH_Drawing_PositionAndAffinity} struct that holds the position and affinity
 * of the glyph cluster.
 * @since 11
 * @version 1.0
 */
OH_Drawing_PositionAndAffinity* OH_Drawing_TypographyGetGlyphPositionAtCoordinateWithCluster(OH_Drawing_Typography*,
    double, double);

/**
 * @brief Obtains the position attribute of an <b>OH_Drawing_PositionAndAffinity</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_PositionAndAffinity Pointer to an <b>OH_Drawing_PositionAndAffinity</b> object,
 * which is obtained by {@link OH_Drawing_TypographyGetGlyphPositionAtCoordinate} or
 * {@link OH_Drawing_TypographyGetGlyphPositionAtCoordinateWithCluster}.
 * @return Returns the position attribute.
 * @since 11
 * @version 1.0
 */
size_t OH_Drawing_GetPositionFromPositionAndAffinity(OH_Drawing_PositionAndAffinity*);

/**
 * @brief Obtains the affinity attribute of an <b>OH_Drawing_PositionAndAffinity</b> object.
 * The affinity determines whether the font is close to the front text or rear text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_PositionAndAffinity Pointer to an <b>OH_Drawing_PositionAndAffinity</b> object.
 * which is obtained by {@link OH_Drawing_TypographyGetGlyphPositionAtCoordinate} or
 * {@link OH_Drawing_TypographyGetGlyphPositionAtCoordinateWithCluster}.
 * @return Returns the affinity attribute.
 * @since 11
 * @version 1.0
 */
int OH_Drawing_GetAffinityFromPositionAndAffinity(OH_Drawing_PositionAndAffinity*);

/**
 * @brief Obtains the word boundary.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypography}.
 * @param size_t Index of the word.
 * @return Returns the {@link OH_Drawing_Range} struct that holds the word boundary.
 * @since 11
 * @version 1.0
 */
OH_Drawing_Range* OH_Drawing_TypographyGetWordBoundary(OH_Drawing_Typography*, size_t);

/**
 * @brief Obtains the start position of an <b>OH_Drawing_Range</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Range Pointer to an <b>OH_Drawing_Range</b> object, which is obtained by
 * {@link OH_Drawing_TypographyGetWordBoundary}.
 * @return Returns the start position.
 * @since 11
 * @version 1.0
 */
size_t OH_Drawing_GetStartFromRange(OH_Drawing_Range*);

/**
 * @brief Obtains the end position of an <b>OH_Drawing_Range</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Range Pointer to an <b>OH_Drawing_Range</b> object, which is obtained by
 * {@link OH_Drawing_TypographyGetWordBoundary}.
 * @return Returns the end position.
 * @since 11
 * @version 1.0
 */
size_t OH_Drawing_GetEndFromRange(OH_Drawing_Range*);

/**
 * @brief Obtains the number of lines.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypography}.
 * @return Returns the number of lines.
 * @since 11
 * @version 1.0
 */
size_t OH_Drawing_TypographyGetLineCount(OH_Drawing_Typography*);

/**
 * @brief Sets the text decoration style.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTextStyle}.
 * @param int Text decoration style. For details about the available options,
 * see {@link OH_Drawing_TextDecorationStyle}.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleDecorationStyle(OH_Drawing_TextStyle*, int);

/**
 * @brief Sets the thickness scale factor of the text decoration line.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTextStyle}.
 * @param double Scale factor.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleDecorationThicknessScale(OH_Drawing_TextStyle*, double);

/**
 * @brief Sets the letter spacing for text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTextStyle}.
 * @param double Letter spacing.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleLetterSpacing(OH_Drawing_TextStyle*, double);

/**
 * @brief Sets the word spacing for text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTextStyle}.
 * @param double Word spacing.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleWordSpacing(OH_Drawing_TextStyle*, double);

/**
 * @brief Sets half leading for text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTextStyle}.
 * @param bool Whether to enable half leading. The value <b>true</b> means to enable half lading,
 * and <b>false</b> means the opposite.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleHalfLeading(OH_Drawing_TextStyle*, bool);

/**
 * @brief Sets the ellipsis content for text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTextStyle}.
 * @param char* Pointer to the ellipsis content. The data type is a pointer pointing to char.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleEllipsis(OH_Drawing_TextStyle*, const char*);

/**
 * @brief Sets the ellipsis style for text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTextStyle}.
 * @param int Ellipsis style. For details about the available options, see {@link OH_Drawing_EllipsisModal}.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleEllipsisModal(OH_Drawing_TextStyle*, int);

/**
 * @brief Sets the text break strategy.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an <b>OH_Drawing_TypographyStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypographyStyle}.
 * @param int Text break strategy. For details about the available options, see {@link OH_Drawing_BreakStrategy}.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextBreakStrategy(OH_Drawing_TypographyStyle*, int);

/**
 * @brief Sets the word break type.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an <b>OH_Drawing_TypographyStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypographyStyle}.
 * @param int Word break type. For details about the available options, see {@link OH_Drawing_WordBreakType}.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextWordBreakType(OH_Drawing_TypographyStyle*, int);

/**
 * @brief Sets the ellipsis style for text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an <b>OH_Drawing_TypographyStyle</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypographyStyle}.
 * @param int Ellipsis style. For details about the available options, see {@link OH_Drawing_EllipsisModal}.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextEllipsisModal(OH_Drawing_TypographyStyle*, int);

/**
 * @brief Obtains the height of a line.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypography}.
 * @param int Target line.
 * @return Returns the height.
 * @since 11
 * @version 1.0
 */
double OH_Drawing_TypographyGetLineHeight(OH_Drawing_Typography*, int);

/**
 * @brief Obtains the width of a line.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by
 * {@link OH_Drawing_CreateTypography}.
 * @param int Target line.
 * @return Returns the width.
 * @since 11
 * @version 1.0
 */
double OH_Drawing_TypographyGetLineWidth(OH_Drawing_Typography*, int);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
